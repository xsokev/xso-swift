//
//  CalendarAppearance.swift
//  xSoCalendar
//
//  Created by Kevin Armstrong on 2/2/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

struct CalendarAppearance {
    static private let propertyBase = "xSoCalendar"
    static private let defaultColor:UIColor = UIColor.redColor()
    static var firstWeekday:Int {
        get {
            if let first = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"FirstWeekday") as? Int {
                return first
            } else {
                let currentCalendar = NSCalendar.currentCalendar()
                return currentCalendar.firstWeekday
            }
        }
    }
    static var textSize:CGFloat {
        get {
            if let textsize = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"TextSize") as? CGFloat {
                return textsize
            } else {
                return 14
            }
        }
    }
    static var headerColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"HeaderColor") as? String {
                return UIColor(rgba: color)
            } else {
                return defaultColor
            }
        }
    }
    static var currentDayColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"CurrentDayColor") as? String {
                return UIColor(rgba: color)
            } else {
                return defaultColor
            }
        }
    }
    static var currentDayHighlightColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"CurrentDayHighlightColor") as? String {
                return UIColor(rgba: color)
            } else {
                return defaultColor
            }
        }
    }
    static var currentDayHighlightTextColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"CurrentDayHighlightTextColor") as? String {
                return UIColor(rgba: color)
            } else {
                return UIColor.whiteColor()
            }
        }
    }
    static var dayHighlightColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"DayHighlightColor") as? String {
                return UIColor(rgba: color)
            } else {
                return UIColor(rgba: "#efefef")
            }
        }
    }
    static var dayHighlightTextColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"DayHighlightTextColor") as? String {
                return UIColor(rgba: color)
            } else {
                return UIColor.darkTextColor()
            }
        }
    }
    static var dayColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"DayColor") as? String {
                return UIColor(rgba: color)
            } else {
                return UIColor.darkTextColor()
            }
        }
    }
    static var disabledDayColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"DisabledDayColor") as? String {
                return UIColor(rgba: color)
            } else {
                return UIColor(white: 0.0, alpha: 0.1)
            }
        }
    }
    static var otherDayColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"OtherDayColor") as? String {
                return UIColor(rgba: color)
            } else {
                return UIColor(white: 0.0, alpha: 0.3)
            }
        }
    }
}