//
//  CalendarUtils.swift
//  xSoCalendar
//
//  Created by Kevin Armstrong on 1/31/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import Foundation
import UIKit

class CalendarUtils {
//    DATE UTILS
    class func monthWeeks(date:NSDate) -> [Int: [CalendarDate]] {
        let monthStart = date.startOfMonth()
        let monthEnd = date.endOfMonth()
        var start = CalendarDate(date: monthStart.subtractDays(monthStart.weekday-1))
        var weeks: [Int: [CalendarDate]] = [Int: [CalendarDate]]()
        for w in 1...max(date.weeks, 5) {
            var days:[CalendarDate] = [CalendarDate]()
            for d in 1...7 {
//                if calendarDate.day <= endComps.day && (w > 1 || d >= startComps.weekday) {
                if !monthStart.isMonth(start.date) {
                    start.isCurrentMonth = false
                }
                days.append(start)
                start = start.next()
            }
            weeks.updateValue(days, forKey: w)
        }
        return weeks
    }
    
//    BORDER UTILS
    class func bottomBorder(size:CGSize, color:UIColor) -> CALayer {
        let border:CALayer = CALayer()
        border.frame = CGRectMake(0, size.height, size.width, 0.5)
        border.borderColor = color.CGColor
        border.borderWidth = 0.5
        return border
    }
    class func topBorder(size:CGSize, color:UIColor) -> CALayer {
        let border:CALayer = CALayer()
        border.frame = CGRectMake(0, 0, size.width, 0.5)
        border.borderColor = color.CGColor
        border.borderWidth = 0.5
        return border
    }
    class func leftBorder(size:CGSize, color:UIColor) -> CALayer {
        let border:CALayer = CALayer()
        border.frame = CGRectMake(0, 0, 0.5, size.height)
        border.borderColor = color.CGColor
        border.borderWidth = 0.5
        return border
    }
    class func rightBorder(size:CGSize, color:UIColor) -> CALayer {
        let border:CALayer = CALayer()
        border.frame = CGRectMake(size.width, 0, 0.5, size.height)
        border.borderColor = color.CGColor
        border.borderWidth = 0.5
        return border
    }
}