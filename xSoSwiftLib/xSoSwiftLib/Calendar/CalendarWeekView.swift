//
//  CalendarWeekView.swift
//  xSoCalendar
//
//  Created by Kevin Armstrong on 1/31/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

class CalendarWeekView: UIView {
    private var border:CALayer?
    
    var dayViews:[CalendarDayView] = [CalendarDayView]()
    
    convenience init(frame: CGRect, calendarView:CalendarView, days:[CalendarDate]) {
        self.init(frame: frame)
        self.border = CalendarUtils.topBorder(self.frame.size, color: UIColor(white: 0.0, alpha: 0.1))
        self.layer.addSublayer(self.border)
        let width = self.frame.width / 7
        for i in 0...6 {
            let x:CGFloat = CGFloat(i) * width
            let day:CalendarDate? = days[i]
            if day != nil {
                var dv = CalendarDayView(frame: CGRectMake(x, 0, width, frame.size.height), calendateDate: day!)
                dv.delegate = calendarView
                self.addSubview(dv)
                self.dayViews.append(dv)
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func destroy(){
        for dayView in self.subviews as [CalendarDayView] {
            dayView.removeFromSuperview()
        }
        self.border?.removeFromSuperlayer()
        self.removeFromSuperview()
    }
}