//
//  CalendarDate.swift
//  xSoCalendar
//
//  Created by Kevin Armstrong on 2/2/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import Foundation

struct CalendarDate {
    var date:NSDate!
    var isCurrent = false
    var isCurrentMonth = true
    
    init() {
        self.init(date:NSDate())
    }
    init(date:NSDate) {
        self.date = date
        self.isCurrent = (date == NSDate())
    }
    func next() -> CalendarDate {
        return CalendarDate(date: self.date.addDays(1))
    }
    func prev() -> CalendarDate {
        return CalendarDate(date: self.date.subtractDays(1))
    }
}

extension CalendarDate: NilLiteralConvertible {
    init(nilLiteral: ()){ }
}