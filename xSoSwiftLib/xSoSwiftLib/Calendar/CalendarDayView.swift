//
//  CalendarDayView.swift
//  xSoCalendar
//
//  Created by Kevin Armstrong on 1/31/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

protocol CalendarDayViewDelegate {
    func didTapDate(dateView:CalendarDayView)
}

class CalendarDayView: UIView {
    private var label:UILabel?
    private var circleView:UIView?
    
    var calendateDate:CalendarDate?
    var delegate:CalendarDayViewDelegate?
    var enabled:Bool = true

    convenience init (frame: CGRect, calendateDate:CalendarDate) {
        self.init(frame: frame)
        self.calendateDate = calendateDate
        self.label = UILabel(frame: CGRectMake(0, 0, frame.size.width, frame.size.height))
        self.label!.text = calendateDate.date.day.description
        self.label!.font = UIFont.systemFontOfSize(CalendarAppearance.textSize)
        self.label!.textAlignment = NSTextAlignment.Center
        self.setEnabled(true)
        self.addSubview(self.label!)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: "dayViewTapped")
        self.addGestureRecognizer(tapRecognizer)
    }
    func dayViewTapped() {
        if self.enabled {
            self.delegate?.didTapDate(self)
        }
    }
    func setEnabled(value:Bool){
        self.enabled = value
        if self.enabled {
            if self.calendateDate?.isCurrent == true {
                self.label!.font = UIFont.boldSystemFontOfSize(CalendarAppearance.textSize)
                self.label!.textColor = CalendarAppearance.currentDayHighlightColor
            } else if self.calendateDate?.isCurrentMonth == false {
                self.label!.textColor = CalendarAppearance.otherDayColor
            } else {
                self.label!.textColor = CalendarAppearance.dayColor
            }
        } else {
            self.label!.textColor = CalendarAppearance.disabledDayColor
        }
    }
    func setDayLabelHighlighted(animate:Bool) {
        var color:UIColor!
        var labelFrame = CGRectMake(0, 0, self.label!.frame.width, self.label!.frame.height)
        if self.enabled {
            if self.calendateDate?.isCurrent == true {
                color = CalendarAppearance.currentDayHighlightColor
                self.label!.textColor = CalendarAppearance.currentDayHighlightTextColor
            } else {
                color = CalendarAppearance.dayHighlightColor
                self.label!.textColor = CalendarAppearance.dayHighlightTextColor
            }
            self.circleView = CalendarDayHighlightView(frame: labelFrame, color: color!, _alpha: 1.0)
            self.insertSubview(self.circleView!, atIndex: 0)
            
            if animate == true {
                self.label?.transform = CGAffineTransformMakeScale(0.5, 0.5)
                self.circleView?.transform = CGAffineTransformMakeScale(0.5, 0.5)
                
                UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.BeginFromCurrentState, animations: { () -> Void in
                    self.circleView?.transform = CGAffineTransformMakeScale(1, 1)
                    self.label?.transform = CGAffineTransformMakeScale(1, 1)
                    
                    }) { (Bool) -> Void in
                        
                }
            }
        }
    }
    
    func setDayLabelUnhighlighted(animate:Bool) {
        if animate == true {
            UIView.animateWithDuration(0.25, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                self.circleView?.transform = CGAffineTransformMakeScale(0.1, 0.1)
                self.circleView?.alpha = 0.0
            }) { (Bool) -> Void in
                self.unhighlight()
            }
        } else {
            self.unhighlight()
        }
    }
    private func unhighlight(){
        var color: UIColor!
        if self.calendateDate?.isCurrent == true {
            color = CalendarAppearance.currentDayColor
        } else if self.calendateDate?.isCurrentMonth == false {
            color = CalendarAppearance.otherDayColor
        } else {
            color = CalendarAppearance.dayColor
        }
        self.label!.textColor = color
        self.circleView?.removeFromSuperview()
        self.circleView = nil
    }
    func destroy(){
        self.delegate = nil
        self.circleView?.removeFromSuperview()
        self.circleView = nil
        self.label!.removeFromSuperview()
        self.removeFromSuperview()
    }
}
