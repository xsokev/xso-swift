//
//  xSoCalendar.swift
//  xSoCalendar
//
//  Created by Kevin Armstrong on 1/30/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

protocol CalendarViewDelegate {
    func didSelectDate(date:NSDate)
}

class CalendarView: UIView, UIScrollViewDelegate, CalendarDayViewDelegate {
    enum ScrollDirection {
        case None
        case Left
        case Right
    }
    
    private let daysHeaderView:UIView = UIView()
    private let daysHeaderViewHeight:CGFloat = 25
    private let calendarView:UIScrollView = UIScrollView()

    private var border:CALayer?
    private var monthViews: [Int:CalendarMonthView]?
    private var lastContentOffset: CGFloat = 0
    private var page:Int = 1
    private var pageChanged = false
    private var pageLoadingEnabled = true
    private var direction: ScrollDirection = .None
    private var togglingBlocked = false
    private var presentedMonth = NSDate()
    private var selectedDateView:CalendarDayView?
    
    var currentDate = NSDate()
    var maxDate:NSDate?
    var minDate:NSDate?
    var delegate:CalendarViewDelegate?

    convenience init(startdate:NSDate) {
        self.init()
        self.setDate(startdate)
    }
    convenience init(startdate:NSDate, frame: CGRect) {
        self.init(frame: frame)
        self.setDate(startdate)
    }

    override init() {
        super.init()
        self.createDaySymbols()
        self.createCalendarView()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.createDaySymbols()
        self.createCalendarView()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    deinit {
        self.daysHeaderView.removeFromSuperview()
        for (index,monthView) in self.monthViews! {
            monthView.destroy()
        }
        self.border?.removeFromSuperlayer()
        self.calendarView.removeFromSuperview()
    }
    
    
//    PUBLIC METHODS
    func updateLayout(){
        self.updateDaySymbols()
        self.updateCalendarView()
    }
    func setDate(newdate:NSDate) {
        self.currentDate = newdate
        self.setMonthView(newdate)
    }
    func setMax(maxDate:NSDate) {
        self.maxDate = maxDate
        self.doLimits()
    }
    func setMin(minDate:NSDate) {
        self.minDate = minDate
        self.doLimits()
    }
    func clearMax() {
        self.maxDate = nil
        self.doLimits()
    }
    func clearMin() {
        self.minDate = nil
        self.doLimits()
    }
    
//    DELEGATED METHODS
    func didTapDate(dateView: CalendarDayView) {
        var previousDate = self.currentDate
        var newDate:NSDate!
        if let date = dateView.calendateDate?.date {
            newDate = date
        }
        if previousDate != newDate {
            if self.selectedDateView != nil {
                self.selectedDateView!.setDayLabelUnhighlighted(true)
            }
            dateView.setDayLabelHighlighted(true)
            self.selectedDateView = dateView
            if !self.currentDate.isMonth(newDate) {
                self.setMonthView(newDate)
            }
            self.currentDate = newDate
            self.delegate?.didSelectDate(newDate)
        }
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let width = self.frame.width
        let page = Int(floor((scrollView.contentOffset.x - width/2) / width) + 1)
        if page != self.page {
            self.page = page
            if !self.pageChanged {
                self.pageChanged = true
            } else {
                self.pageChanged = false
            }
        }
        if scrollView.contentOffset.y != 0 {
            scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, 0)
        }
        if self.lastContentOffset > scrollView.contentOffset.x {
            self.direction = .Right
        } else if self.lastContentOffset < scrollView.contentOffset.x {
            self.direction = .Left
        }
        self.lastContentOffset = scrollView.contentOffset.x
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if self.pageChanged {
            if self.direction == .Left {
                if self.monthViews![0] != nil {
                    self.scrolledLeft()
                }
            } else if self.direction == .Right {
                if self.monthViews![2] != nil {
                    self.scrolledRight()
                }
            }
        }
        self.pageChanged = false
        self.pageLoadingEnabled = true
        self.direction = .None
    }


//    PRIVATE METHODS
    private func createDaySymbols() {
        self.daysHeaderView.frame = CGRectMake(0, 0, self.frame.width, daysHeaderViewHeight)
        let dateFormatter = NSDateFormatter()
        var weekdays = dateFormatter.shortWeekdaySymbols as NSArray
        
        let firstWeekdayIndex = CalendarAppearance.firstWeekday - 1
        if (firstWeekdayIndex > 0) {
            let copy = weekdays
            weekdays = (weekdays.subarrayWithRange(NSMakeRange(firstWeekdayIndex, 7 - firstWeekdayIndex)))
            weekdays = weekdays.arrayByAddingObjectsFromArray(copy.subarrayWithRange(NSMakeRange(0, firstWeekdayIndex)))
        }
        
        let symbols = weekdays as [String]
        let space:CGFloat = 0
        let width = self.frame.width / 7 - space
        
        var x: CGFloat = 0
        
        for i in 0..<7 {
            x = CGFloat(i) * width + space
            
            let symbol = UILabel(frame: CGRectMake(x, 0, width, daysHeaderViewHeight))
            symbol.textAlignment = .Center
            symbol.text = (symbols[i]).uppercaseString
            symbol.font = UIFont.boldSystemFontOfSize(10)
            symbol.textColor = UIColor.darkTextColor()
            
            self.daysHeaderView.addSubview(symbol)
        }
        self.addSubview(self.daysHeaderView)
        self.border = CalendarUtils.bottomBorder(self.daysHeaderView.frame.size, color: UIColor(white: 0.0, alpha: 0.1))
        self.layer.addSublayer(self.border)
    }
    private func updateDaySymbols(){
        self.daysHeaderView.frame = CGRectMake(0, 0, self.frame.width, daysHeaderViewHeight)
        let width = self.frame.width / 7
        var i:CGFloat = 0
        for symbolView:UIView in self.daysHeaderView.subviews as [UIView] {
            let x:CGFloat = CGFloat(i) * width
            symbolView.frame = CGRectMake(x, 0, width, daysHeaderViewHeight)
            i++
        }
        self.border?.frame = CGRectMake(0, self.daysHeaderView.frame.size.height, self.daysHeaderView.frame.size.width, 0.5)
    }
    private func createCalendarView(){
        self.calendarView.frame = CGRectMake(0, self.daysHeaderViewHeight, self.frame.width, self.frame.height-self.daysHeaderViewHeight)
        self.calendarView.contentSize = CGSizeMake(self.frame.width * 3, self.frame.height)
        self.calendarView.showsHorizontalScrollIndicator = false
        self.calendarView.showsVerticalScrollIndicator = false
        self.calendarView.pagingEnabled = true
        self.calendarView.delegate = self
        self.monthViews = [Int:CalendarMonthView]()

        let previousMonth = self.getPreviousMonthView(self.presentedMonth)
        let startMonth = self.getPresentedMonthView(self.presentedMonth)
        let nextMonth = self.getNextMonthView(self.presentedMonth)
        
        self.monthViews!.updateValue(previousMonth, forKey: 0)
        self.monthViews!.updateValue(startMonth, forKey: 1)
        self.monthViews!.updateValue(nextMonth, forKey: 2)
        
        self.insertMonthView(previousMonth, atIndex: self.page - 1)
        self.insertMonthView(startMonth, atIndex: self.page)
        self.insertMonthView(nextMonth, atIndex: self.page + 1)

        self.selectDayView(self.currentDate)
        self.addSubview(self.calendarView)
        self.calendarView.scrollRectToVisible(startMonth.frame, animated: false)
    }
    private func updateCalendarView(){
        self.calendarView.frame = CGRectMake(0, self.daysHeaderViewHeight, self.frame.width, self.frame.height-self.daysHeaderViewHeight)
        self.calendarView.contentSize = CGSizeMake(self.frame.width * 3, self.frame.height)

        for (index,monthView) in self.monthViews! {
            monthView.destroy()
        }
        self.monthViews = [Int:CalendarMonthView]()
        
        let previousMonth = self.getPreviousMonthView(self.presentedMonth)
        let startMonth = self.getPresentedMonthView(self.presentedMonth)
        let nextMonth = self.getNextMonthView(self.presentedMonth)
        
        self.monthViews!.updateValue(previousMonth, forKey: 0)
        self.monthViews!.updateValue(startMonth, forKey: 1)
        self.monthViews!.updateValue(nextMonth, forKey: 2)
        
        self.insertMonthView(previousMonth, atIndex: self.page - 1)
        self.insertMonthView(startMonth, atIndex: self.page)
        self.insertMonthView(nextMonth, atIndex: self.page + 1)
        
        self.selectDayView(self.currentDate)
        self.calendarView.scrollRectToVisible(startMonth.frame, animated: false)
    }
    private func insertMonthView(monthView: UIView, atIndex index: Int) {
        let width = self.calendarView.contentSize.width / 3
        let height = self.calendarView.frame.height
        let x = CGFloat(index) * width
        let y = CGFloat(0)
        let frame = CGRectMake(x, y, width, height)
        monthView.frame = frame
        
        self.calendarView.addSubview(monthView)
    }
    private func replaceMonthView(monthView:UIView, toPage page: Int, animatable: Bool) {
        var frame = monthView.frame
        frame.origin.x = frame.width * CGFloat(page)
        monthView.frame = frame
        if animatable {
            self.calendarView.scrollRectToVisible(frame, animated: false)
        }
    }
    private func scrolledLeft() {
        if self.page != 1 && self.pageLoadingEnabled {
            self.pageLoadingEnabled = false
            
            let leftMonthView = self.monthViews![1]
            let presentedView = self.monthViews![2]
            let newDate = presentedView?.date
            
            self.replaceMonthView(leftMonthView!, toPage: 0, animatable: false)
            self.replaceMonthView(presentedView!, toPage: 1, animatable: true)
            
            var extraMonthView:CalendarMonthView? = self.monthViews!.removeValueForKey(0)
            extraMonthView!.destroy()
            
            let rightMonthView = self.getNextMonthView(newDate!)
            
            self.monthViews!.updateValue(leftMonthView!, forKey: 0)
            self.monthViews!.updateValue(presentedView!, forKey: 1)
            self.monthViews!.updateValue(rightMonthView, forKey: 2)
            
            self.insertMonthView(rightMonthView, atIndex: 2)
            self.presentedMonth = newDate!
            if self.currentDate.isMonth(newDate!) {
                self.selectDayView(self.currentDate)
            }
        }
    }
    private func scrolledRight() {
        if self.page != 1 && self.pageLoadingEnabled {
            self.pageLoadingEnabled = false
            
            let rightMonthView = self.monthViews![1]
            let presentedView = self.monthViews![0]
            let newDate = presentedView?.date
            
            self.replaceMonthView(rightMonthView!, toPage: 2, animatable: false)
            self.replaceMonthView(presentedView!, toPage: 1, animatable: true)
            
            var extraMonthView:CalendarMonthView? = self.monthViews!.removeValueForKey(2)
            extraMonthView!.destroy()
            
            let leftMonthView = self.getPreviousMonthView(newDate!)
            
            self.monthViews!.updateValue(leftMonthView, forKey: 0)
            self.monthViews!.updateValue(presentedView!, forKey: 1)
            self.monthViews!.updateValue(rightMonthView!, forKey: 2)
            
            self.insertMonthView(leftMonthView, atIndex: 0)
            self.presentedMonth = newDate!
            if self.currentDate.isMonth(newDate!) {
                self.selectDayView(self.currentDate)
            }
        }
    }
    private func setMonthView(date: NSDate) {
        var currentMonthView = self.monthViews![1]
        if self.presentedMonth != date && !self.togglingBlocked {
            self.togglingBlocked = true
            let presentedMonthView = self.getPresentedMonthView(date)
            let rightMonthView = self.getNextMonthView(date)
            let leftMonthView = self.getPreviousMonthView(date)
            
            var extraLeftMonthView = self.monthViews!.removeValueForKey(0)
            var extraRightMonthView = self.monthViews!.removeValueForKey(2)
            
            extraLeftMonthView!.destroy()
            extraRightMonthView!.destroy()
            
            let frame = CGRectMake(0, 0, self.calendarView.frame.width, self.calendarView.frame.height)
            presentedMonthView.alpha = 0
            
            self.monthViews!.removeValueForKey(1)
            
            self.monthViews!.updateValue(leftMonthView, forKey: 0)
            self.monthViews!.updateValue(presentedMonthView, forKey: 1)
            self.monthViews!.updateValue(rightMonthView, forKey: 2)

            self.insertMonthView(presentedMonthView, atIndex: 1)
            self.selectDayView(date)
            
            UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                    presentedMonthView.alpha = 1
                    currentMonthView?.alpha = 0
                }) { (finished) -> Void in
                    currentMonthView!.destroy()
                    
                    self.insertMonthView(leftMonthView, atIndex: 0)
                    self.insertMonthView(rightMonthView, atIndex: 2)
                    
                    self.presentedMonth = date
                    self.togglingBlocked = false
            }
        } else {
            self.selectDayView(date)
        }
    }
    private func selectDayView(date:NSDate){
        var currentMonthView = self.monthViews![1]
        if self.selectedDateView != nil {
            self.selectedDateView!.setDayLabelUnhighlighted(false)
            self.selectedDateView = nil
        }
        var canSelect = true
        if self.minDate != nil {
            if date < self.minDate {
                canSelect = false
            }
        }
        if self.maxDate != nil {
            if date > self.maxDate {
                canSelect = false
            }
        }
        if canSelect {
            self.selectedDateView = currentMonthView?.findDateView(date)?
            self.selectedDateView?.setDayLabelHighlighted(false)
        }
    }
    private func doLimits(){
        for (index,monthView) in self.monthViews! {
            monthView.setLimits(self.minDate?, max: self.maxDate?)
        }
        self.selectDayView(self.currentDate)
    }

//    MONTH VIEW METHODS
    private func getNextMonthView(date: NSDate) -> CalendarMonthView {
        return self.getMonthView(date.addMonths(1))
    }
    private func getPreviousMonthView(date: NSDate) -> CalendarMonthView {
        return self.getMonthView(date.subtractMonths(1))
    }
    private func getPresentedMonthView(date: NSDate) -> CalendarMonthView {
        return self.getMonthView(date)
    }
    private func getMonthView(date: NSDate) -> CalendarMonthView {
        let frame = CGRectMake(0, 0, self.frame.width, self.calendarView.frame.height)
        return CalendarMonthView(frame: frame, calendarView:self, date:date)
    }
}
