//
//  CalendarDayHighlightView.swift
//  xSoCalendar
//
//  Created by Kevin Armstrong on 2/2/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

class CalendarDayHighlightView: UIView {
    private let color: UIColor?
    init(frame: CGRect, color: UIColor, _alpha: CGFloat) {
        super.init(frame: frame)
        self.color = color
        self.alpha = _alpha
        self.backgroundColor = .clearColor()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func drawRect(rect: CGRect) {
        var context = UIGraphicsGetCurrentContext()
        
        CGContextSetLineWidth(context, 0.5)
        var radius = (frame.width > frame.height) ? frame.height : frame.width
        CGContextAddArc(context, (frame.size.width)/2, frame.size.height/2, (radius - 10)/2, 0.0, CGFloat(M_PI * 2.0), 1)
        
        CGContextSetFillColorWithColor(context, self.color!.CGColor)
        CGContextSetStrokeColorWithColor(context, self.color!.CGColor)
        CGContextDrawPath(context, kCGPathFillStroke)
    }
}
