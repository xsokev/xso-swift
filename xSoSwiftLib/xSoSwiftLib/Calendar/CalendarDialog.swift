//
//  CalendarDialog.swift
//  xso-swift
//
//  Created by Kevin Armstrong on 2/9/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

protocol CalendarDialogDelegate {
    func didSelectDate(date:NSDate)
}

class CalendarDialog: Dialog, CalendarViewDelegate {

    let calendar:CalendarView = CalendarView()
    
    var closeOnSelection:Bool = false
    var calendarDelegate:CalendarDialogDelegate?
    
    convenience init(parent:UIView, frame:CGRect, startdate:NSDate=NSDate()){
        self.init(frame: CGRectZero)
        self.parent = parent
        self.calendar.delegate = self
        self.calendar.frame = frame
        self.calendar.updateLayout()
        self.calendar.setDate(startdate)
    }
    func display(title:String="", canClose:Bool=true, buttons:[DialogButton]=[DialogButton]()){
        if buttons.count == 0 {
            self.showFooter = false
        }
        self.display(self.calendar, title: title, canClose: canClose, buttons: buttons)
    }
    func didSelectDate(date: NSDate) {
        self.calendarDelegate?.didSelectDate(date)
        if self.closeOnSelection {
            self.dismissModal()
        }
    }
}
