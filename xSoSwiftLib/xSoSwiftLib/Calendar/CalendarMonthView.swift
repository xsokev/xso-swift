//
//  CalendarMonthView.swift
//  xSoCalendar
//
//  Created by Kevin Armstrong on 1/31/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

class CalendarMonthView: UIView {
    private var header:UILabel?
    private var weekHeight:CGFloat?
    private var weekViews:[CalendarWeekView] = [CalendarWeekView]()
    
    var date:NSDate?
    
    convenience init(frame: CGRect, calendarView:CalendarView, date:NSDate){
        self.init(frame: frame)

        let weeks = CalendarUtils.monthWeeks(date)
        self.date = date
        self.weekHeight = self.frame.height / CGFloat(weeks.count+1)
        
        createLabel()
        for week in weeks {
            let weekView = CalendarWeekView(frame: CGRectMake(0, self.weekHeight!*CGFloat(week.0), self.frame.width, self.weekHeight!), calendarView: calendarView, days: week.1)
            self.addSubview(weekView)
            self.weekViews.append(weekView)
        }
        self.setLimits(calendarView.minDate?, max: calendarView.maxDate?)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func setLimits(min:NSDate?, max:NSDate?){
        for weekView:CalendarWeekView in self.weekViews {
            for view:CalendarDayView in weekView.dayViews {
                if min != nil && view.calendateDate?.date < min! {
                    view.setEnabled(false)
                } else if max != nil && view.calendateDate?.date > max! {
                    view.setEnabled(false)
                } else {
                    view.setEnabled(true)
                }
            }
        }
    }
    func findDateView(fordate:NSDate) -> CalendarDayView? {
        var dayView:CalendarDayView?
        for weekView:CalendarWeekView in self.weekViews {
            for view:CalendarDayView in weekView.dayViews {
                if view.calendateDate!.date == fordate {
                    dayView = view
                }
            }
        }
        return dayView?
    }
    func destroy(){
        self.header!.removeFromSuperview()
        for weekView in self.subviews as [CalendarWeekView] {
            weekView.destroy()
        }
        self.removeFromSuperview()
    }
    private func createLabel() {
        self.header = UILabel(frame: CGRectMake(0, 0, self.frame.width, self.weekHeight!))
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MMMM YYYY"
        self.header!.text = formatter.stringFromDate(self.date!)
        self.header!.font = UIFont.boldSystemFontOfSize(16)
        self.header!.textAlignment = NSTextAlignment.Center
        self.header!.textColor = CalendarAppearance.headerColor
        self.addSubview(self.header!)
    }
}
