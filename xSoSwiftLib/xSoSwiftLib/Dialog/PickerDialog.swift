//
//  PickerDialog.swift
//  xso-swift
//
//  Created by Kevin Armstrong on 2/9/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

protocol PickerDialogDelegate {
    func didSelectValue(value:(component:Int, value:String))
}

class PickerDialog: Dialog, UIPickerViewDataSource, UIPickerViewDelegate {
    
    let picker:UIPickerView = UIPickerView()
    
    private var pickerData = []
    
    var value:(component:Int, value:String)?
    var pickerDelegate:PickerDialogDelegate?
    
    convenience init(parent:UIView){
        self.init(frame: CGRectZero)
        self.parent = parent
        self.picker.delegate = self
        self.picker.dataSource = self
        self.picker.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width-20, 180)
    }
    func setData(data:[AnyObject]){
        pickerData = data
    }
    func display(title:String="", canClose:Bool=true){
        var buttons = [
            DialogButton(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace),
            DialogButton(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("doDone"))
        ]
        self.display(self.picker, title: title, canClose: canClose, buttons: buttons)
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData[component].count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickerData[component][row] as String
    }
//    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        let titleData = pickerData[component][row] as String
//        var myTitle = NSAttributedString(string: titleData, attributes: [
//            NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,
//            NSForegroundColorAttributeName:UIColor.blackColor()
//        ])
//        return myTitle
//    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.value = (component, pickerData[component][row] as String)
    }
    func doDone(){
        self.pickerDelegate?.didSelectValue(self.value!)
        self.dismissModal()
    }
}
