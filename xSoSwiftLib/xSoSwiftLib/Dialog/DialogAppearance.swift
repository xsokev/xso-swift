//
//  DialogAppearance.swift
//  xso-swift
//
//  Created by Kevin Armstrong on 2/3/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

struct DialogAppearance {
    static private let propertyBase = "xSoDialog"
    static private let white = UIColor.whiteColor()
    static private let light = UIColor(white: 0.0, alpha: 0.1)
    static private let dark = UIColor(white: 0.0, alpha: 0.7)
    static private let black = UIColor.blackColor()

    static var background:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"Background") as? String {
                return UIColor(rgba: color)
            } else {
                return white
            }
        }
    }
    static var headerColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"HeaderColor") as? String {
                return UIColor(rgba: color)
            } else {
                return light
            }
        }
    }
    static var headerTextColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"HeaderTextColor") as? String {
                return UIColor(rgba: color)
            } else {
                return dark
            }
        }
    }
    static var footerColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"FooterColor") as? String {
                return UIColor(rgba: color)
            } else {
                return light
            }
        }
    }
    static var maskBackground:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"MaskBackground") as? String {
                return UIColor(rgba: color)
            } else {
                return UIColor(white: 0.0, alpha: 0.5)
            }
        }
    }
    static var radius:CGFloat {
        get {
            if let value = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"Radius") as? CGFloat {
                return value
            } else {
                return 4.0
            }
        }
    }
    static var shadowColor:UIColor {
        get {
            if let color = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"ShadowColor") as? String {
                return UIColor(rgba: color)
            } else {
                return black
            }
        }
    }
    static var shadow:Bool {
        get {
            if let value = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"Shadow") as? Bool {
                return value
            } else {
                return true
            }
        }
    }
    static var shadowOpacity:Float {
        get {
            if let value = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"ShadowOpacity") as? Float {
                return value
            } else {
                return 0.5
            }
        }
    }
    static var shadowOffset:CGSize {
        get {
            var width:CGFloat = 0.0
            var height:CGFloat = 3.0
            if let w = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"ShadowOffsetWidth") as? CGFloat {
                width = w
            }
            if let h = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"ShadowOffsetHeight") as? CGFloat {
                height = h
            }
            return CGSizeMake(width, height)
        }
    }
    static var shadowRadius:CGFloat {
        get {
            if let value = NSBundle.mainBundle().objectForInfoDictionaryKey(propertyBase+"ShadowRadius") as? CGFloat {
                return value
            } else {
                return 4.0
            }
        }
    }
}