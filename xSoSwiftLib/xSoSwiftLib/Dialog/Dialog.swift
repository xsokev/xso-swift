//
//  ModalDialog.swift
//  Dialog
//
//  Created by Kevin Armstrong on 1/27/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

protocol DialogDelegate {
    func didCloseModal()
}

class Dialog: UIView {
    private let headerHeight:CGFloat = 32.0
    private var mask:UIView?
    private var body:UIView?
    private var header:UINavigationBar = UINavigationBar()
    private var headerTitle:UINavigationItem = UINavigationItem()
    private var footer:UIToolbar = UIToolbar()
    
    var animationSpeed:CGFloat = 0.4
    var shadow:Bool = true
    var blurBackground = false
    var dismissOnTouchOutside = true
    var showHeader = true
    var showFooter = true
    var delegate:DialogDelegate!
    var parent:UIView!
    
    convenience init(parent:UIView){
        self.init(frame: CGRectZero)
        self.parent = parent
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func display(body:UIView, title:String="", canClose:Bool=true, buttons:[DialogButton]=[DialogButton]()){
        self.makeHeader(title, canClose: canClose, width: body.frame.width)
        self.makeFooter(buttons, width: body.frame.width)
        self.body = body
        self.display()
    }
    func display(){
        if let body = self.body? {
            let screenBounds = UIScreen.mainScreen().bounds
            let parentSize:CGSize = screenBounds.size
            
            self.backgroundColor = DialogAppearance.background
            self.layer.cornerRadius = DialogAppearance.radius
            if self.blurBackground {
                self.mask = UIToolbar(frame: screenBounds)
                (self.mask as UIToolbar).barStyle = UIBarStyle.Default
            } else {
                self.mask = UIView(frame: screenBounds)
                self.mask?.backgroundColor = DialogAppearance.maskBackground
            }
            if dismissOnTouchOutside {
                self.mask?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("detectMaskTap:")))
            }
            self.mask?.alpha = 0.0
            
            if self.shadow {
                self.layer.shadowColor = DialogAppearance.shadowColor.CGColor
                self.layer.shadowOpacity = DialogAppearance.shadowOpacity
                self.layer.shadowRadius = DialogAppearance.shadowRadius
                self.layer.shadowOffset = DialogAppearance.shadowOffset
            }
            var modalSize:CGSize = body.frame.size
            if self.showHeader {
                modalSize.height = modalSize.height + self.header.frame.height
                self.addSubview(self.header)
                body.frame.origin = CGPoint(x: 0, y: self.header.frame.height)
            } else {
                body.frame.origin = CGPoint(x: 0, y: 0)
            }
            self.addSubview(body)
            if self.showFooter && self.footer.items?.count > 0 {
                modalSize.height = modalSize.height + self.footer.frame.height
                self.footer.frame.origin = CGPoint(x: 0, y: body.frame.origin.y + body.frame.height)
                self.addSubview(self.footer)
            }
            self.frame = CGRectMake((parentSize.width-modalSize.width)/2, (parentSize.height-modalSize.height)/2, modalSize.width, modalSize.height)
            self.mask?.addSubview(self)
            self.parent!.addSubview(self.mask!)
            
            if let mask = self.mask {
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    mask.alpha = 1.0
                })
            }
        } else {
            fatalError("ModalDialog must have a body!!")
        }
    }
    func setTitle(title:String) {
        (self.headerTitle.titleView as UILabel).text = title
    }
    func dismissModal(){
        if let mask = self.mask {
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                mask.alpha = 0.0
                }) { (completed:Bool) -> Void in
                    self.removeFromSuperview()
                    self.body?.removeFromSuperview()
                    self.header.removeFromSuperview()
                    self.footer.removeFromSuperview()
                    self.mask?.removeFromSuperview()
                    self.body = nil
                    self.mask = nil
            }
        }
        self.delegate?.didCloseModal()
    }
    func detectMaskTap(gesture:UIGestureRecognizer){
        if let mask = self.mask {
            var tap:UITapGestureRecognizer = gesture as UITapGestureRecognizer
            var point:CGPoint = tap.locationInView(mask)
            if !CGRectContainsPoint(self.frame, point) {
                self.dismissModal()
            }
        }
    }
    
    private func makeHeader(title:String, canClose:Bool, width:CGFloat) {
        self.header.popNavigationItemAnimated(false)
        let navItem = self.headerTitle
        let titleView = UILabel(frame: CGRectMake(0, 0, width, headerHeight))
        titleView.text = title
        titleView.textAlignment = NSTextAlignment.Left
        titleView.textColor = DialogAppearance.headerTextColor
        titleView.font = UIFont.systemFontOfSize(14)
        navItem.titleView = titleView
        
        if canClose {
            var closeBtn:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Stop, target: self, action: Selector("dismissModal"))
            closeBtn.setTitleTextAttributes([ NSFontAttributeName: UIFont.systemFontOfSize(12)], forState: UIControlState.Normal)
            navItem.rightBarButtonItem = closeBtn
        }
        self.header.frame = CGRectMake(0, 0, width, headerHeight)
        self.header.pushNavigationItem(navItem, animated: false)
    }
    private func makeFooter(buttons:[DialogButton], width:CGFloat) {
        self.footer.setItems(buttons, animated: false)
        self.footer.frame.size = CGSize(width: width, height: headerHeight)
    }
}
