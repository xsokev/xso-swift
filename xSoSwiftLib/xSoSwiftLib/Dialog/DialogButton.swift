//
//  DialogButton.swift
//  xso-swift
//
//  Created by Kevin Armstrong on 2/3/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

class DialogButton: UIBarButtonItem {
    override init() {
        super.init()
    }
    convenience init(barButtonSystemItem systemItem: UIBarButtonSystemItem) {
        self.init(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
    }
    override init(barButtonSystemItem systemItem: UIBarButtonSystemItem, target: AnyObject?, action: Selector) {
        super.init(barButtonSystemItem: systemItem, target: target, action: action)
    }
    init(title: String?, target: AnyObject?, action: Selector) {
        super.init(title: title, style: UIBarButtonItemStyle.Plain, target: target, action: action)
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
