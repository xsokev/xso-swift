//
//  NSDate-extensions.swift
//  xso-swift
//
//  Created by Kevin Armstrong on 2/5/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//
//  Copied from NSDate by Piergiuseppe Longo on 23/11/14.
//  Copyright (c) 2014 pNre. All rights reserved.
//

import Foundation

public extension NSDate {
    
    // MARK:  NSDate Manipulation
    
    /**
    Returns a new NSDate object representing the date calculated by adding the amount specified to self date
    
    :param: seconds number of seconds to add
    :param: minutes number of minutes to add
    :param: hours number of hours to add
    :param: days number of days to add
    :param: weeks number of weeks to add
    :param: months number of months to add
    :param: years number of years to add
    :returns: the NSDate computed
    */
    public func add(seconds: Int = 0, minutes: Int = 0, hours: Int = 0, days: Int = 0, weeks: Int = 0, months: Int = 0, years: Int = 0) -> NSDate {
        var calendar = NSCalendar.currentCalendar()
        var date = calendar.dateByAddingUnit(.CalendarUnitSecond, value: seconds, toDate: self, options: nil) as NSDate!
        date = calendar.dateByAddingUnit(.CalendarUnitMinute, value: minutes, toDate: date, options: nil)
        date = calendar.dateByAddingUnit(.CalendarUnitDay, value: days, toDate: date, options: nil)
        date = calendar.dateByAddingUnit(.CalendarUnitHour, value: hours, toDate: date, options: nil)
        date = calendar.dateByAddingUnit(.CalendarUnitWeekOfMonth, value: weeks, toDate: date, options: nil)
        date = calendar.dateByAddingUnit(.CalendarUnitMonth, value: months, toDate: date, options: nil)
        date = calendar.dateByAddingUnit(.CalendarUnitYear, value: years, toDate: date, options: nil)
        return date
    }
    public func startOfDay() -> NSDate {
        var calendar = NSCalendar.currentCalendar()
        return calendar.dateBySettingHour(0, minute: 0, second: 0, ofDate: self, options: nil)!
    }
    public func startOfMonth() -> NSDate {
        var calendar = NSCalendar.currentCalendar()
        var comps = calendar.components(NSCalendarUnit.MonthCalendarUnit | NSCalendarUnit.YearCalendarUnit | NSCalendarUnit.DayCalendarUnit, fromDate: self)
        comps.day = 1
        return calendar.dateFromComponents(comps)!
    }
    public func endOfMonth() -> NSDate {
        return self.startOfMonth().add(months: 1).subtractDays(1)
    }
    public func startOfYear() -> NSDate {
        var calendar = NSCalendar.currentCalendar()
        var comps = calendar.components(NSCalendarUnit.MonthCalendarUnit | NSCalendarUnit.YearCalendarUnit | NSCalendarUnit.DayCalendarUnit, fromDate: self.startOfMonth())
        comps.month = 1
        return calendar.dateFromComponents(comps)!
    }
    
    
//    CONVENIENCE METHODS
    public func addSeconds (seconds: Int) -> NSDate {
        return add(seconds: seconds)
    }
    public func addMinutes (minutes: Int) -> NSDate {
        return add(minutes: minutes)
    }
    public func addHours(hours: Int) -> NSDate {
        return add(hours: hours)
    }
    public func addDays(days: Int) -> NSDate {
        return add(days: days)
    }
    public func addWeeks(weeks: Int) -> NSDate {
        return add(weeks: weeks)
    }
    public func addMonths(months: Int) -> NSDate {
        return add(months: months)
    }
    public func addYears(years: Int) -> NSDate {
        return add(years: years)
    }
    public func subtractSeconds (seconds: Int) -> NSDate {
        return add(seconds: -1*seconds)
    }
    public func subtractMinutes (minutes: Int) -> NSDate {
        return add(minutes: -1*minutes)
    }
    public func subtractHours(hours: Int) -> NSDate {
        return add(hours: -1*hours)
    }
    public func subtractDays(days: Int) -> NSDate {
        return add(days: -1*days)
    }
    public func subtractWeeks(weeks: Int) -> NSDate {
        return add(weeks: -1*weeks)
    }
    public func subtractMonths(months: Int) -> NSDate {
        return add(months: -1*months)
    }
    public func subtractYears(years: Int) -> NSDate {
        return add(years: -1*years)
    }
    
    // MARK:  Date comparison
    public func isAfter(date: NSDate) -> Bool{
        return (self.compare(date) == NSComparisonResult.OrderedDescending)
    }
    public func isBefore(date: NSDate) -> Bool{
        return (self.compare(date) == NSComparisonResult.OrderedAscending)
    }
    public func isMonth(date:NSDate) -> Bool {
        return NSCalendar.currentCalendar().compareDate(self, toDate: date, toUnitGranularity: .MonthCalendarUnit) == NSComparisonResult.OrderedSame
    }
    public func isYear(date:NSDate) -> Bool {
        return NSCalendar.currentCalendar().compareDate(self, toDate: date, toUnitGranularity: .YearCalendarUnit) == NSComparisonResult.OrderedSame
    }
    
    
    // MARK: Getters
    public var year : Int {
        get {
            return getComponent(.CalendarUnitYear)
        }
    }
    public var month : Int {
        get {
            return getComponent(.CalendarUnitMonth)
        }
    }
    public var weekday : Int {
        get {
            return getComponent(.CalendarUnitWeekday)
        }
    }
    public var weekMonth : Int {
        get {
            return getComponent(.CalendarUnitWeekOfMonth)
        }
    }
    public var day : Int {
        get {
            return getComponent(.CalendarUnitDay)
        }
    }
    public var hours : Int {
        
        get {
            return getComponent(.CalendarUnitHour)
        }
    }
    public var minutes : Int {
        get {
            return getComponent(.CalendarUnitMinute)
        }
    }
    public var seconds : Int {
        get {
            return getComponent(.CalendarUnitSecond)
        }
    }
    public var weeks : Int {
        get {
            return NSCalendar.currentCalendar().rangeOfUnit(.WeekCalendarUnit, inUnit: .MonthCalendarUnit, forDate: self).length
        }
    }
    
    /**
    Returns the value of the NSDate component
    
    :param: component NSCalendarUnit
    :returns: the value of the component
    */
    
    public func getComponent (component : NSCalendarUnit) -> Int {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(component, fromDate: self)
        
        return components.valueForComponent(component)
    }
}

extension NSDate: Strideable {
    public func distanceTo(other: NSDate) -> NSTimeInterval {
        return other - self
    }
    
    public func advancedBy(n: NSTimeInterval) -> Self {
        return self.dynamicType(timeIntervalSinceReferenceDate: self.timeIntervalSinceReferenceDate + n)
    }
}

// MARK: Arithmetic

func +(date: NSDate, timeInterval: Int) -> NSDate {
    return date + NSTimeInterval(timeInterval)
}

func -(date: NSDate, timeInterval: Int) -> NSDate {
    return date - NSTimeInterval(timeInterval)
}

func +=(inout date: NSDate, timeInterval: Int) {
    date = date + timeInterval
}

func -=(inout date: NSDate, timeInterval: Int) {
    date = date - timeInterval
}

func +(date: NSDate, timeInterval: Double) -> NSDate {
    return date.dateByAddingTimeInterval(NSTimeInterval(timeInterval))
}

func -(date: NSDate, timeInterval: Double) -> NSDate {
    return date.dateByAddingTimeInterval(NSTimeInterval(-timeInterval))
}

func +=(inout date: NSDate, timeInterval: Double) {
    date = date + timeInterval
}

func -=(inout date: NSDate, timeInterval: Double) {
    date = date - timeInterval
}

func -(date: NSDate, otherDate: NSDate) -> NSTimeInterval {
    return date.timeIntervalSinceDate(otherDate)
}

extension NSDate: Equatable {}

public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.startOfDay().compare(rhs.startOfDay()) == NSComparisonResult.OrderedSame
}
public func ===(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == NSComparisonResult.OrderedSame
}

extension NSDate: Comparable {}

public func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == NSComparisonResult.OrderedAscending
}