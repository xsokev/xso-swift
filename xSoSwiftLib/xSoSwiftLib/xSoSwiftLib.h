//
//  xSoSwiftLib.h
//  xSoSwiftLib
//
//  Created by Kevin Armstrong on 2/10/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for xSoSwiftLib.
FOUNDATION_EXPORT double xSoSwiftLibVersionNumber;

//! Project version string for xSoSwiftLib.
FOUNDATION_EXPORT const unsigned char xSoSwiftLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <xSoSwiftLib/PublicHeader.h>


