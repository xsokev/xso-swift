//
//  SecondViewController.swift
//  xSo.Swift
//
//  Created by Kevin Armstrong on 2/3/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, CalendarDialogDelegate {
    @IBOutlet weak var startDate: UITextField!
    @IBOutlet weak var birthDate: UITextField!
    @IBOutlet weak var bRange: UIButton!
    @IBOutlet weak var lblRange: UILabel!
    
    
    private var cal:CalendarView?
    private var dlg:Dialog?
    private var calDlg:CalendarDialog?
    
    
    @IBAction func doStartDate(sender: AnyObject) {
        var buttons = [
            DialogButton(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace),
            DialogButton(title: "Done", target: self, action: Selector("doStartDone"))
        ]
        self.dlg?.display(self.cal!, title: "Select Start Date", canClose: true, buttons: buttons)
        if self.startDate.text != "" {
            var formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
            self.cal?.setDate(formatter.dateFromString(self.startDate.text)!)
        }
    }
    @IBAction func doBirthDate(sender: AnyObject) {
        self.calDlg?.closeOnSelection = true
        self.calDlg?.calendarDelegate = self
        self.calDlg?.display(title: "Birth Date", canClose: true)
        if self.birthDate.text != "" {
            var formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
            self.calDlg?.calendar.setDate(formatter.dateFromString(self.birthDate.text)!)
        }
    }
    @IBAction func doRange(sender: AnyObject) {
        var buttons = [
            DialogButton(title: "Max", target: self, action: Selector("setMax")),
            DialogButton(title: "Min", target: self, action: Selector("setMin")),
            DialogButton(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace),
            DialogButton(title: "Done", target: self, action: Selector("doRangeDone"))
        ]
        self.dlg?.display(self.cal!, title: "Date in Range", canClose: true, buttons: buttons)
        self.cal?.setMax(NSDate().addDays(9))
        self.cal?.setMin(NSDate().subtractDays(9))
        if self.lblRange.text != "" {
            var formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
            self.cal?.setDate(formatter.dateFromString(self.lblRange.text!)!)
        }
    }
    
    func didCloseModal() {
//        do something here after modal closes. Only works if ModalDelegate is set to this view controller
    }
    func setToday(){
        self.cal?.setDate(NSDate())
    }
    func setMax(){
        self.cal?.setMax(NSDate().addDays(90))
    }
    func setMin(){
        self.cal?.setMin(NSDate().subtractDays(90))
    }
    func setDOB(){
        var units = NSCalendarUnit.DayCalendarUnit | NSCalendarUnit.MonthCalendarUnit | NSCalendarUnit.YearCalendarUnit
        var components = NSCalendar.currentCalendar().components(units, fromDate: NSDate())
        components.month = 9
        components.year = 1970
        components.day = 13
        self.cal!.setDate(NSCalendar.currentCalendar().dateFromComponents(components)!)
    }
    func doRangeDone(){
        var date = self.cal?.currentDate
        var formatter:NSDateFormatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        self.lblRange.text = formatter.stringFromDate(date!)
        self.dlg?.dismissModal()
    }
    func doStartDone(){
        var date = self.cal?.currentDate
        var formatter:NSDateFormatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        self.startDate.text = formatter.stringFromDate(date!)
        self.dlg?.dismissModal()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dlg = Dialog(parent: self.view)
        self.calDlg = CalendarDialog(parent: self.view, frame: CGRectMake(0, 0, 300, 300))
        self.cal = CalendarView(startdate: NSDate(), frame: CGRectMake(0, 0, 300, 300))
        self.bRange.layer.cornerRadius = 5.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func didSelectDate(date: NSDate) {
        var formatter:NSDateFormatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        self.birthDate.text = formatter.stringFromDate(date)
        self.calDlg?.dismissModal()
    }
}

