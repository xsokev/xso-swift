//
//  FirstViewController.swift
//  xSo.Swift
//
//  Created by Kevin Armstrong on 2/3/15.
//  Copyright (c) 2015 kevinandre. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, PickerDialogDelegate {
    
    @IBOutlet weak var lbl1: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func doValue1(sender: AnyObject) {
        let dlg = PickerDialog(parent: self.view)
        dlg.pickerDelegate = self
        dlg.setData([["1 Week", "2 Weeks", "3 Weeks", "4 Weeks", "5 Weeks"]])
        dlg.display(title: "Time Frame", canClose: false)
    }
    
    func didSelectValue(value: (component: Int, value: String)) {
        self.lbl1.text = value.value
    }

}

